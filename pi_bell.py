#!/usr/bin/python
# -*- coding: latin-1 -*-

__author__ = "Julien Bonnier <j.bonnier@gmail.com>"
__copyright__ = "Copyright 2017, Julien Bonnier"
__license__ = "GPL"
__maintainer__ = "Julien Bonnier"
__email__ = "j.bonnier@gmail.com"

__date__ = "$2017-02-21 13:53$"
__version__ = "0.0.0.20170221.1353"
__status__ = "Production"

# Import des modules
from datetime import datetime
from threading import Timer
import time
import sys
import atexit

try:
	import RPi.GPIO as GPIO
except ImportError as e:
	print "RPi GPIO module not installed."
	print "Application will abort."
	sys.exit()
except:
	print "Unexpected error:", sys.exc_info()[0]
	sys.exit()

# Declaration de variables
## pin 11 = coupe, pin 12 = atelier
bell_pins = [12, 11]	#gpio 17, 18
ring_time = 4		#secondes

# Initialisation de la numerotation et des E/S
def setupGPIO():
	GPIO.setmode(GPIO.BOARD)
	for pin in bell_pins:
		GPIO.setup(pin, GPIO.OUT, initial = GPIO.LOW)

def init():
	setupGPIO()
	ring()

# On fait sonner la cloche.
def ring():
	for pin in bell_pins:
		GPIO.output(pin, GPIO.HIGH)
		time.sleep(ring_time)
		GPIO.output(pin, GPIO.LOW)
#	for pin in bell_pins:
#		GPIO.output(pin, GPIO.LOW)
	
def quit():
	print "Exiting..."
	try:
		GPIO.cleanup()
		print "GPIO cleaned up!"
	except:
		print "Unexpected error:", sys.exc_info()[0]
		
if __name__ == '__main__': 
	init()

atexit.register(quit)
